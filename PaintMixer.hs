--module PaintMixer where

import Data.List
import Data.Maybe (fromMaybe)
import Control.Monad.State    -- cabal install mtl

data Shine = G | M deriving (Show, Eq)
data Color = Color Int Shine deriving (Show, Eq)
data Customer = Customer { choices :: [Color] } deriving (Show)

type MatteCount = Int
type Solution = Maybe ([Color], MatteCount)
type PaintChooserState = (Solution, Solution)
solve :: [Customer] -> State PaintChooserState Solution
solve [] = do
  (currSln, bestSln) <- get
  return $ chooseSolution currSln bestSln
-- solve (x:xs) = do
--   st <- get
--   foldl (\curr choice ->
--     if choiceAvailable choice currPath
--       then solve xs
--       else acc) st (choices x))



solve2 :: [Customer] -> State PaintChooserState Solution
solve2 [] = do
  return Nothing
-- solve2 cs = do
--   return $ solve' cs Nothing
--
-- solve' :: [Customer] -> Solution -> Solution
-- solve' [] _ = do
--   (Just currSln', Just bestSln) <- get
--   return $ chooseSolution (Just currSln') bestSln
-- solve' (x:xs) = do
--   (Just (currSln, currMatteCount), bestSln) <- get
--   Just $ foldl (\curr choice ->
--     if choiceAvailable choice curr
--       then solve xs
--       else curr) currSln (choices x)

type PaintMixingSolution = Maybe ([Color], Int)
type PaintMixingState = (PaintMixingSolution, PaintMixingSolution)
type PaintMixingStateMonad = State PaintMixingSolution

test_customers1 = [
  Customer { choices = [Color 1 M] },
  Customer { choices = [Color 1 G, Color 2 M, Color 3 G] },
  Customer { choices = [Color 2 G, Color 3 G] }]

-- solve4 :: Int -> [Customer] -> PaintMixingStateMonad PaintMixingSolution
-- solve4 n customers = solve4' n customers ([], 0)
-- solve4 (x:xs) = do
--   st <- get
--   return st
--   foldl (\(current, best) color -> do
--     -- case current of Nothing -> put (Just (color:[], 0), best)
--     --                 _       -> put (nextState current color, best)
--     --put newState
--     (current, best)) st (choices x)
--   st
  -- x

-- solve4' :: Int -> [Customer] -> ([Color], Int) -> PaintMixingStateMonad PaintMixingSolution
-- solve4' n [] curr = do
--   best <- get
--   return $ chooseSolution' (Just curr) best
--
-- solve4' n (cus:xcustomers) (colors, currMatteCount) = do
--   foldl (\acc color ->
--     if choiceAvailable color colors
--       then solve4' n xcustomers (color:colors, currMatteCount + (if isMatte color then 1 else 0))
--       else acc) Nothing (choices cus)

nextState :: PaintMixingSolution -> Color -> PaintMixingSolution
nextState Nothing color = Just ([color], 0)


chooseSolution' :: PaintMixingSolution -> PaintMixingSolution -> PaintMixingSolution
chooseSolution' curr Nothing = curr
chooseSolution' (Just (currColors, currMatteCount)) (Just (bestColors, bestMatteCount)) =
    if currMatteCount < bestMatteCount
      then Just (currColors, currMatteCount)
      else Just (bestColors, bestMatteCount)

isMatte :: Color -> Bool
isMatte (Color _ M) = True
isMatte _           = False


solve5 :: Int -> Int -> [Color] -> [Customer] -> [[Color]]
solve5 p n currPath []     =  currPath
solve5 p n currPath (x:xs) = foldl (\acc choice ->
  if choiceAvailable choice currPath
    then (solve5 p (n+1) (choice:currPath) xs):acc
    else acc) [] (choices x)

--main = print $ evalState (solve4 3 test_customers1) Nothing
main = solve5 3 0 [] test_customers1

-- valFromState :: PaintMixingStateMonad -> PaintMixingSolution
-- valFromState (_, s) = s

-- nextState :: PaintMixingSolution -> PaintMixingSolution
-- solve3 :: [Customer] -> PaintMixingStateMonad PaintMixingSolution
-- solve3 [] = return Nothing
-- solve3 xs = solve3' xs Nothing
--
-- solve3' :: [Customer] -> PaintMixingSolution -> PaintMixingStateMonad PaintMixingSolution
-- solve3' [] Nothing = return Nothing
-- solve3' [] (Just (currColors, currMatteCount)) = do
--   bestSln <- get
--   put $ case bestSln of (Nothing) -> Just (currColors, currMatteCount)
--                         (Just (bestColors, bestMatteCount)) ->
--                           if currMatteCount < bestMatteCount
--                             then Just (currColors, currMatteCount)
--                             else bestSln
--   return (Just (currColors, currMatteCount))
-- solve3' (c:cs) currSln  = do
--   return $ foldl (\currSln' (Color i s) -> do
--     return $ case currSln' of (Nothing) -> solve3' cs (Just ([(Color i s)], if s == M then 1 else 0))
--                              (Just (colors, matteCount)) ->
--                               if choiceAvailable (Color i s) colors
--                                 then solve3' cs (Just ((Color i s):colors, matteCount + (if s == M then 1 else 0)))
--                                 else Nothing) currSln (choices c)
-- solve3' (c:cs) currentSln = do
--   st <- get
--   --return st
--   return $ foldl (\st' choice -> case st' of (Nothing, _)
--     if choiceAvailable choice )

-- solve3' :: [Customer] -> PaintMixingState PaintMixingSolution
-- solve3

chooseSolution :: Solution -> Solution -> Solution
chooseSolution currPath Nothing = currPath
chooseSolution (Just (currPath, currPathMatteCount)) (Just (slnPath, slnPathMatteCount)) =
  if currPathMatteCount < slnPathMatteCount
    then Just (currPath, currPathMatteCount)
    else Just (slnPath, slnPathMatteCount)

-- solve :: Int -> Int -> [Color] -> [Customer] -> [[Color]]
-- solve p n currPath [] = Nothing
-- solve p n currPath (x:xs) = foldl (\acc choice ->
--   if choiceAvailable choice currPath
--     then acc ++ spaces n ++ show choice ++ " " ++ show n ++ "\n" ++ solve p (n+1) (choice:currPath) xs
--     else acc) "" (choices x)

spaces :: Int -> String
spaces n = intercalate "" $ replicate n " "

print' :: Int -> Int -> [Color] -> [Customer] -> String
print' p n currPath []     = ""
print' p n currPath (x:xs) = foldl (\acc choice ->
  if choiceAvailable choice currPath
    then acc ++ spaces n ++ show choice ++ " " ++ show n ++ "\n" ++ print' p (n+1) (choice:currPath) xs
    else acc) "" (choices x)

choiceAvailable :: Color -> [Color] -> Bool
choiceAvailable c s =
  matchedByColor == Nothing || fromMaybe False (matchesFinish c <$> matchedByColor)
  where matchedByColor = find (\pathColor -> matchesColor c pathColor) s

matchesColor :: Color -> Color -> Bool
matchesColor (Color i _) (Color j _) = i == j

matchesFinish :: Color -> Color -> Bool
matchesFinish (Color _ fi) (Color _ fj) = fi == fj

-- 5            # The first line specifies how many colors there are.
-- 1 M 3 G 5 G  # Each subsequent line describes a customer. For example, this first customer likes color 1 in matte, color 3 in gloss and color 5 in gloss.
-- 2 G 3 M 4 G  # This second customer would like color 2 in gloss, color 3 in matte and color 4 in gloss
-- 5 M          # This third customer would like color 5 in matte
