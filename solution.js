var fs = require('fs');

// read in our testfile's contents, parse it into:
// {
//   n: Integer,
//   customerChoices: Array<Array<string>>
// }
const input = fs.readFileSync(`${__dirname}/${process.argv[process.argv.length - 1]}`, 'utf8')
const parsed = parse(input);

// entry point:
solve(parsed.n, parsed.customerChoices)

// helper function to strip comments from test files
function trimFromHash(line = "") {
  const i = line.indexOf('#');
  return i === -1 ? line : line.substring(0, i).trim();
}

// parse input and return an object containing the input to our `solve` function
function parse(input) {
  var lines = input.split('\n').map(trimFromHash).filter(l => l);
  return {
    n: parseInt(lines[0]),
    customerChoices: lines.slice(1).map(choiceList => choiceList.split(' ').reduce((res, colorOrFinish, i) => {
      const index = Math.floor(i / 2);
      if (!res[index]) {
        res[index] = '';
      }
      res[index] += colorOrFinish;
      return res;
    }, []))
  };
}

// top level call which starts the recursive descent.
// keep track of the ideal solution found so far (if any) in our result variable
function solve(n, customerChoices) {
  var result;

  // recursive helper to keep track of the current "color path"
  // and compare it to the best path found so far (by comparing # of mattes)
  function _solve(customerChoices, currentPath) {

    // base-case: if we've reached the end of the customerChoices,
    // we're guaranteed to have found a valid solution.  if this solution's
    // matte quantity is less than our current best solution, it becomes the
    // new best solution
    if (customerChoices.length === 0) {
      if (!result || matteCount(currentPath) < matteCount(result)) {
        result = currentPath;
      }
      return;
    }

    // successive case: customerChoices is non-empty, so iterate
    // through the customer's choices (i.e., color preferences)
    // and continue deeper IF the choice could be valid (i.e., the choice
    // is not in the current path, OR if it is, its finish matches the current choice)
    customerChoices[0].forEach(choice => {
      var slot = currentPath.find(c => color(c) === color(choice));
      if (!slot || finish(slot) === finish(choice)) {
        _solve(customerChoices.slice(1), currentPath.concat([choice]))
      }
    });

  }

  // initial call to traverse our graph
  _solve(customerChoices, []);

  // display final result
  finalResult(n, result);
}

// format and display the final result/output
// see -- for example -- sample1.test: it is satisfied by X X G G M,
// where Xs are irrelevant.  finalResult is given ['3G', '4G', '5M']
// and returns 'G G G G M'
function finalResult(n, result) {
  if (!result) {
    console.error("No solution exists");
    process.exit(1);
  }

  var formatted = 'G'.repeat(n).split('');
  result.sort().forEach(choice => {
    formatted[color(choice) - 1] = finish(choice)
  });
  console.log(formatted.join(' '))
  process.exit(0);
}

/**
helper functions
**/

function matteCount(path) {
  return path.reduce((count, choice) => finish(choice) === 'M' ? count + 1 : count, 0);
}

function color(choice) {
  return choice[0];
}

function finish(choice) {
  return choice[choice.length - 1];
}
