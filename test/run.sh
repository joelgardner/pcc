#!/bin/bash

X=$(($(ls -1 test/ | wc -l)-1))
echo "Running $X tests (note: assuming filenames following sample#.test convention!)"

for i in $(seq 1 $X)
do
   node solution.js test/sample$i.test
done
